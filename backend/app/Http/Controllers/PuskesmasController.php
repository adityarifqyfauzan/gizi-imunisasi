<?php

namespace App\Http\Controllers;

use App\Puskesmas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PuskesmasController extends Controller
{
    public function index()
    {
        $listPuskesmas = Puskesmas::all();
        return response()->json($listPuskesmas); 
    }

    public function Show($id)
    {
        $puskesmas = Puskesmas::find($id);
        
        if ($puskesmas) {
            return response()->json($puskesmas);
        }else{
            $response = ['message' => 'Data Puskesmas tidak ditemukan'];
            return response()->json($response, 404);
        }
    }

    public function store(Request $request)
    {
        
        $rules = [
            'nama_puskesmas' => 'required|max:12',
            'alamat' => 'required'
        ];

        $message = [
            'required' => 'Silahkan isi :attribute, :attribute tidak boleh kosong',
        ];

        $data = collect($request)->all();

        $validator = Validator::make($data, $rules, $message);
        
        if ($validator->fails()) {
            
            $response = [
                'message' => 'Error Validation',
                'errors' => $validator->errors()->message()
            ];

            return response()->json($response, 400);
        
        }

        $lastId = Puskesmas::count();
        $kode_puskesmas = $data['nama_puskesmas'];
        $data['kode_puskesmas'] = "PUS-" . substr($kode_puskesmas, 0, 3) . sprintf("%05s", $lastId + 1); 
        
        $puskesmas = Puskesmas::create($data);

        $response = ['message' => 'Data Puskesmas berhasil ditambahkan'];

        return response()->json($response);
    }
}
