<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Puskesmas extends Model
{
    protected $primaryKey = 'kode_puskesmas';
    
    protected $table = 'puskesmas';

    public $timestamps = false;

    public $incrementing = false;

    protected $fillable = [
        'kode_puskesmas', 'nama_puskesmas', 'alamat'
    ];

}
