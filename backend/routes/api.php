<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
$router->get('/', function () use ($router) {
    return $router->app->version();
});

// $router->group(['prefix' => 'puskesmas'], function () use ($router){
//     $router->post('/', 'PuskesmasController@create');
//     $router->get('/', 'PuskesmasController@index');
//     $router->get('/{id}', 'PuskesmasController@show');
// });

Route::group(['prefix' => 'puskesmas'], function () {
    Route::get('/', 'PuskesmasController@index');
    Route::get('/{id}', 'PuskesmasController@Show');
    Route::post('/', 'PuskesmasController@store');
});